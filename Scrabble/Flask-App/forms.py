from flask_wtf import FlaskForm
from wtforms import IntegerField
from wtforms import TextField
from wtforms import SelectField
from wtforms import DateTimeField
from wtforms.validators import InputRequired, Length, Email

#Form for creating new members:
class NewMemberForm(FlaskForm):
	surname = TextField('surname', validators=[InputRequired()])
	forename = TextField('forename', validators=[InputRequired()])
	
	phone_number = TextField('phone_number', validators=[InputRequired()])
	email_address = TextField('email_address', validators=[Email()])
	city = TextField('city', validators=[InputRequired()])
	street_name = TextField('street_name', validators=[InputRequired()])
	house_number = IntegerField('house_Number', validators=[InputRequired()])
	postcode = TextField('postcode', validators=[InputRequired(),Length(min=6, max=9, message="User entered invalid postcode.")])


#Form for logging new matches:
class NewMatchForm(FlaskForm):
	player1 = SelectField('player1', coerce=int)
	player2 = SelectField('player2', coerce=int)
	
	player1_score = IntegerField('player1_score')
	player2_score = IntegerField('player2_score')
	
	match_date = DateTimeField('match_date', format="%d/%m/%Y")
	
