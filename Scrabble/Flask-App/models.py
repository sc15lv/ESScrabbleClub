from app import db
from sqlalchemy.dialects.postgresql import JSON

#The Match object represents a scrabble match between 2 players.
class Match(db.Model):
	__tablename__ = 'matches'
	
	#match ID
	id = db.Column(db.Integer, primary_key=True) 
	
	#player ID's
	player1_id = db.Column(db.Integer)
	player2_id = db.Column(db.Integer)

	#player scores:
	player1_score = db.Column(db.Integer)
	player2_score = db.Column(db.Integer)
	
	match_date = db.Column(db.DateTime)

#The Member object represents a single member signed up for the website.
class Member(db.Model):
	__tablename__ = 'members'
	#member ID
	id = db.Column(db.Integer, primary_key=True) 
	
	#Member details:
	forename = db.Column(db.String(64))
	surname = db.Column(db.String(64))
	join_date = db.Column(db.DateTime)
	
	#Address & Contact:
	email_address = db.Column(db.String(128))
	phone_number = db.Column(db.String(13))
	city = db.Column(db.String(64))
	street_name = db.Column(db.String(64))
	house_number = db.Column(db.Integer)
	postcode = db.Column(db.String(8))
	
	
	
