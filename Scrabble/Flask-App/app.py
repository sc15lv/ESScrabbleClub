#!/usr/bin/env python
import os
import config
from datetime import datetime
from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask import render_template, request
from flask_wtf import FlaskForm
from wtforms import TextField
from wtforms import BooleanField
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)
app.config.from_object(config)
db = SQLAlchemy(app)

from models import *
from forms import *

@app.route('/', methods=['GET', 'POST'])
def index():
	#get list of top 10 member score averages, for members who have logged at least 10 matches:

	qResult = db.engine.execute(""" SELECT members.id, round(avg(
									CASE
										WHEN matches.player1_id IS members.id THEN matches.player1_score
										WHEN matches.player2_id IS members.id THEN matches.player2_score
									END),0) AS avgScore,
									members.forename || ' ' || members.surname AS 'Name'
									
									FROM members INNER JOIN  matches ON 
									CASE
										WHEN matches.player1_id IS members.id THEN matches.player1_id
										WHEN matches.player2_id IS members.id THEN matches.player2_id
									END
									WHERE (SELECT COUNT(*) FROM matches WHERE player1_id IS members.id OR player2_id IS members.id) >= 10
									GROUP BY members.id
									ORDER BY avgScore DESC
									LIMIT 10; """)

	#Convert returned object into list of dictionaries:
	resultDicts = []
	#Extract dictionaries from query result:
	for result in qResult:
		resultDict = {
			'id'   : result[0],
			'avgScore' : result[1],
			'name' : result[2],
		}
		resultDicts.append(resultDict)
	return render_template('index.html', records=resultDicts)

@app.route('/new-member', methods=['GET', 'POST'])
def new_member():
	form = NewMemberForm()
	if form.validate_on_submit():
		#Create new member object, add it to database, and commit changes:
		memberRecord = Member(forename=form.forename.data, surname=form.surname.data, phone_number=form.phone_number.data, city=form.city.data, street_name=form.street_name.data, house_number=form.house_number.data, postcode=form.postcode.data, join_date=datetime.now(), email_address=form.email_address.data)
		db.session.add(memberRecord)
		db.session.commit()
	else:
		print("Something went wrong..")
	return render_template('new-member.html', form=form)

@app.route('/new-entry', methods=['GET', 'POST'])
def new_entry():
	
	#Generate list of members for use in dropdown boxes on match logging page:
	members = Member.query.all()
	membersNames = [(x.id,"%s %s" % (x.forename, x.surname)) for x in members ]
	form = NewMatchForm()
	form.player1.choices = membersNames
	form.player2.choices = membersNames
	
	if form.validate_on_submit():
		#Ensure that the user didn't choose the same player twice for 1 match:
		if form.player1.data == form.player2.data:
			print("Match cannot have single player.")
		else:
			#Create a new match entry, add it to the database, and commit changes:
			matchRecord = Match(player1_id=form.player1.data, player2_id=form.player2.data, player1_score=form.player1_score.data, player2_score=form.player2_score.data, match_date=form.match_date.data)
			
			db.session.add(matchRecord)
			db.session.commit()
			
	else:
		print("Something went wrong..")
	return render_template('new-entry.html', form=form)

@app.route('/member/<id>', methods=['GET', 'POST'])
def member(id):
	#Get the number of wins for the user matching the given ID:
	wins = db.engine.execute("""SELECT count(id) AS 'Wins' 
								FROM matches 
								WHERE (matches.player1_id IS $id OR  matches.player2_id IS $id) AND 
								CASE
									WHEN matches.player1_id IS $id THEN matches.player1_score >= matches.player2_score
									WHEN matches.player2_id IS $id THEN matches.player2_score >  matches.player1_score
								END;""".replace("$id", id)).first()[0]

	#Get the number of losses for the user matching the given ID:
	losses = db.engine.execute("""SELECT count(id) AS 'Losses' 
								  FROM matches 
								  WHERE (matches.player1_id IS $id OR  matches.player2_id IS $id) AND 
								  CASE
								  	  WHEN matches.player1_id IS $id THEN matches.player1_score < matches.player2_score
								  	  WHEN matches.player2_id IS $id THEN matches.player2_score <= matches.player1_score
								  END;""".replace("$id", id)).first()[0]
	
	#get the average score for the user matching the given ID:
	average = db.engine.execute(""" SELECT avg(
									CASE
										WHEN matches.player1_id IS $id THEN matches.player1_score 
										WHEN matches.player2_id IS $id THEN matches.player2_score
									END)
									FROM matches WHERE (matches.player1_id IS $id OR  matches.player2_id IS $id);""".replace("$id", id)).first()[0]
	
	#Get the highest score attained by the user:
	highest = db.engine.execute(""" SELECT matches.id, match_date,
										max(CASE
											WHEN matches.player1_id IS $id THEN player1_score 
											WHEN matches.player2_id IS $id THEN player2_score
												
										END) AS score, members.forename || ' ' || members.surname AS Opponent
									FROM matches INNER JOIN members ON  members.id =
										CASE 
											WHEN matches.player1_id IS $id THEN player2_id
											WHEN matches.player2_id IS $id THEN player1_id
										END;""".replace("$id", id)).first()
	
	#Get the member's details.
	memberRecord = Member.query.filter_by(id=id).first()
	#Check to make sure the player has played at least 1 match, and if not, generate placeholder data.
	if wins + losses == 0:
		member_dict = {
			'id' : memberRecord.id,
			'name': memberRecord.forename + ' ' + memberRecord.surname,
			'wins': 0,
			'losses': 0,
			'average': 0,
			'highestScore': 0,
			'highestDate': 'never',
			'highestOpponent': 'Nobody',
		}
	else:
		#Store results in dictionary for use in flask template.
		member_dict = {
			'id' : memberRecord.id,
			'name': memberRecord.forename + ' ' + memberRecord.surname,
			'wins': wins,
			'losses': losses,
			'average': round(average),
			'highestScore': highest[2],
			'highestDate': highest[1][:10],
			'highestOpponent': highest[3],
		}
	return render_template('member.html', member=member_dict)

@app.route('/edit-member/<id>', methods=['GET', 'POST'])
def edit_member(id):
	#get user record for given ID:
	user = db.engine.execute("SELECT * FROM members WHERE id IS %s" % id).first() 
	#Create a form and pro-populate it with the details from the existing user record:
	form = NewMemberForm(obj=user)

	#Using a list of keys for both the user and the form, update the user record using the form fields:
	user_keys = user.keys()
	update_dict = {}
	if form.validate_on_submit():
		for key in form.data.keys():
			if key in user_keys:
				update_dict[key] = form.data[key]
		db.session.query(Member).filter(Member.id == id).update(update_dict)
		db.session.commit()
	return render_template('edit-member.html', form = form)

@app.route('/members', methods=['GET', 'POST'])
def members():
	members = db.engine.execute("SELECT id, forename || ' ' || surname FROM members ORDER BY forename, surname DESC;")
	memberDicts = []
	#Extract dictionaries from query result:
	for record in members:
		memberDict = {
			'id'   : record[0],
			'name' : record[1]
		}
		memberDicts.append(memberDict)
		
	#Get number of users:
	userCount = db.engine.execute("SELECT COUNT(*) FROM members;").first()[0]
	return render_template('members.html', members=memberDicts, userCount=userCount)

if __name__ == '__main__':
	app.run()
