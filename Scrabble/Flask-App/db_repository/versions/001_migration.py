from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
matches = Table('matches', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('player1_id', Integer),
    Column('player2_id', Integer),
    Column('player1_score', Integer),
    Column('player2_score', Integer),
    Column('match_date', DateTime),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['matches'].columns['player1_id'].create()
    post_meta.tables['matches'].columns['player2_id'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['matches'].columns['player1_id'].drop()
    post_meta.tables['matches'].columns['player2_id'].drop()
