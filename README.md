#### How To Run ####
The app runs in a python virtual environment; simply run the following commands, or run the bash script titled run_app.sh:

>`source Scrabble/bin/activate`

>`python3 Scrabble/Flask-App/app.py`


Or:
>`./run_app.sh`

Outside of its virtual environment the demo has the following dependencies:
* flask
* flask-bootstrap
* flask-sqlalchemy
* sqlalchemy-migrate
* flask-wtf

